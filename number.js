const str = "one,two,three,four,one,one";

const deck = [];
const count = {};

for (let value of str.split(",")) {
  deck.push(value);
}

console.log(deck);
console.log(deck.length);

for (i = 0; i < deck.length; i++) {
  count[deck[i]] = count[deck[i]] ? count[deck[i]] + 1 : 1;
}

console.log(count);
